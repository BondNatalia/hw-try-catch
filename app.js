//1. коли ми хочемо, щоб код виконувався до кінця, навіть якщо є помилка;також допомагає знайти помилку або вивести повідомлення про помилку
//
const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];
const div = document.getElementById("root");
const ul = document.createElement('ul');
div.append(ul);

books.forEach((book) => {
    try {
        // if (book.author && book.name && book.price) {
        if (book.author) {
            if (book.name) {
                if (book.price) {
                    const li = document.createElement('li');
                    const author = document.createElement("p");
                    author.textContent = book.author;
                    const name = document.createElement("p");
                    name.textContent = book.name;
                    const price = document.createElement("p");
                    price.textContent = book.price;
                    li.append(author);
                    li.append(name);
                    li.append(price);
                    ul.append(li);
                } else {
                    throw new Error("Error:price is undefined")
                }
            } else {
                throw new Error("Error:name is undefined")
            }
        } else {
            throw new Error("Error:author is undefined")
        }
    } catch (e) {
        console.log(e.message);
    }
});



